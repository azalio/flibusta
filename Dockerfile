FROM golang:alpine as builder
WORKDIR /go/src/gitlab.com/azalio/flibusta
ADD . /go/src/gitlab.com/azalio/flibusta/
RUN apk add git mysql-client
RUN go get github.com/tools/godep
RUN godep restore
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .
RUN mkdir /app
RUN cp main /app/
WORKDIR /app
