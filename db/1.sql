SELECT Lang, BookId, Title, AvtorId, FirstName, MiddleName, LastName,
SUM(MatchTitle), SUM(MatchLastName), SUM(MatchFirstName), SUM(MatchMiddleName),
SUM(MatchTitle) + SUM(MatchLastName) + SUM(MatchFirstName) + SUM(MatchMiddleName)
FROM (
SELECT Lang, FileType, Deleted, BookId, Title, AvtorId, FirstName, MiddleName, LastName,
MATCH (Title) AGAINST ('над пропастью во ржи') as MatchTitle,
0 AS MatchLastName, 0 AS MatchMiddleName, 0 AS MatchFirstName
FROM libbook
JOIN libavtor USING(BookId)
                                JOIN libavtorname USING(AvtorId)
                            WHERE (MATCH (Title) AGAINST ('над пропастью во ржи*' IN BOOLEAN MODE))
                            UNION
                            SELECT Lang, FileType, Deleted, BookId, Title, AvtorId, FirstName, MiddleName, LastName,
                                0 AS MatchTitle,
                                MATCH (LastName) AGAINST ('над пропастью во ржи') as MatchLastName,
                                MATCH (FirstName) AGAINST ('над пропастью во ржи') as MatchFirstName,
                                MATCH (MiddleName) AGAINST ('над пропастью во ржи') as MatchMiddleName
                            FROM libbook
                                JOIN libavtor USING(BookId)
                                JOIN libavtorname USING(AvtorId)
                            WHERE
                                MATCH (LastName) AGAINST ('над пропастью во ржи*' IN BOOLEAN MODE) OR
                                MATCH (FirstName) AGAINST ('над пропастью во ржи*' IN BOOLEAN MODE) OR
                                MATCH (MiddleName) AGAINST ('над пропастью во ржи*' IN BOOLEAN MODE)
                        ) AS books
                        WHERE (Deleted = 0 AND Lang = 'ru')
                        GROUP BY BookId, Title, AvtorId, FirstName, MiddleName, LastName
                        ORDER BY SUM(MatchTitle) + SUM(MatchLastName) + SUM(MatchFirstName) + SUM(MatchMiddleName) DESC, LastName, Title
                        LIMIT 200;

SELECT BookId, Title, Title1, Lang, AvtorId, FirstName, MiddleName, LastName
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title) 
	AGAINST ('+над +пропастью +во +ржи' IN BOOLEAN MODE) 
    AND (FileType = 'fb2' OR FileType = 'epub' OR FileType = 'mobi');

SELECT BookId, Title, Title1, Lang, FirstName, MiddleName, LastName
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title, FirstName, MiddleName, LastName) 
	AGAINST ('+над +пропастью +во +ржи' IN BOOLEAN MODE) 
    AND (FileType = 'fb2' OR FileType = 'epub' OR FileType = 'mobi');
	ORDER BY CASE Title1 WHEN 'litres' THEN 1 ELSE 2 END, Ver desc;


 --add index
ALTER TABLE `libbook` ADD INDEX `bookTitle` (`Title`);
ALTER TABLE `libavtorname` ADD INDEX `avtorname` (`MiddleName`);
ALTER TABLE `libavtorname` ADD INDEX `avtorlastname` (`LastName`);

--add full text indexing
CREATE FULLTEXT INDEX libbook_title_full_text_idx ON libbook(Title);
CREATE FULLTEXT INDEX libavtorname_lastname_full_text_idx ON libavtorname(LastName);
CREATE FULLTEXT INDEX libavtorname_middlename_full_text_idx ON libavtorname(MiddleName);
CREATE FULLTEXT INDEX libavtorname_firstname_full_text_idx ON libavtorname(FirstName);

SELECT BookId, Title, Title1, Lang, FirstName, MiddleName, LastName
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title, FirstName, MiddleName, LastName) 
	AGAINST ('+гранатовый +браслет' IN BOOLEAN MODE);

SELECT BookId, GROUP_CONCAT(DISTINCT FirstName, ' ', MiddleName, ' ', LastName SEPARATOR ', ') 
AS AUTHOR FROM libavtor JOIN libavtorname USING(AvtorId) WHERE BookId=378978;

SELECT BookId, Title, Title1, Lang, 
GROUP_CONCAT(DISTINCT FirstName, ' ', MiddleName, ' ', LastName SEPARATOR ', ') AS AUTHOR
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title) 
	AGAINST ('+гранатовый +браслет' IN BOOLEAN MODE)
    GROUP BY Title;

SELECT BookId, Title, Title1, Lang, 
GROUP_CONCAT(FirstName SEPARATOR ','),
GROUP_CONCAT(MiddleName SEPARATOR ','),
GROUP_CONCAT(LastName SEPARATOR ',')
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title) 
	AGAINST ('+гранатовый +браслет' IN BOOLEAN MODE)
GROUP BY BookId;

SELECT BookId, Title, Title1, Lang, 
GROUP_CONCAT(DISTINCT FirstName, ' ', MiddleName, ' ', LastName SEPARATOR ', ') AS author
	FROM libbook 
    JOIN libavtor USING(BookId)
    JOIN libavtorname USING(AvtorId)
	WHERE MATCH (title, author) 
	AGAINST ('+гранатовый +браслет' IN BOOLEAN MODE)
GROUP BY BookId;