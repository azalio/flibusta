package db

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	// without the underscore _, you will get imported but not
	// used error message
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/azalio/flibusta/util"
)

func dbConn(host string, port uint, user string, pass string, name string) (*sql.DB, error) {
	log.Println("Trying to connect to database")
	// [username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
	// example config :
	// user:password@tcp(127.0.0.1:3306)/database
	connectStr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", user, pass, host, port, name)
	// log.Println(connectStr)
	db, err := sql.Open("mysql", connectStr)
	// db, err := sql.Open("mysql", "flibusta:flibusta@tcp(127.0.0.1:3306)/flibusta")

	if err != nil {
		return nil, err
	}
	// defer db.Close()
	return db, nil
}

// Select from db and return response
func Select(book string) ([]util.MySQLResult, error) {

	var queryResults []util.MySQLResult
	var finalQueryResult []util.MySQLResult
	originalQuery := book
	db, err := dbConn(util.Config.DB.Host, util.Config.DB.Port,
		util.Config.DB.User, util.Config.DB.Password, util.Config.DB.Name)

	if err != nil {
		return queryResults, err
	}

	// 'aaa bbb' -> '+aaa +bbb'
	// util.PlusString(&book)
	// log.Println(book)

	// query := fmt.Sprintf(`SELECT BookId, Title, Title1, Lang
	// FROM libbook
	// WHERE MATCH (title)
	// AGAINST ('%s' IN BOOLEAN MODE) AND (FileType = 'fb2' OR FileType = 'epub' OR FileType = 'mobi')
	// ORDER BY CASE Title1 WHEN 'litres' THEN 1 ELSE 2 END, Ver desc;`, book)
	// println(query)
	// query := fmt.Sprintf(`SELECT BookId, Title, Title1, Lang, FirstName, MiddleName, LastName
	// FROM libbook
	// JOIN libavtor USING(BookId)
	// JOIN libavtorname USING(AvtorId)
	// WHERE MATCH (title, FirstName, MiddleName, LastName)
	// AGAINST ('%s' IN BOOLEAN MODE);`, book)
	query := fmt.Sprintf(`SELECT BookId, Title, Title1, Lang, 
	GROUP_CONCAT(DISTINCT FirstName, ' ', MiddleName, ' ', LastName SEPARATOR ', ') AS Author
		FROM libbook 
		JOIN libavtor USING(BookId)
		JOIN libavtorname USING(AvtorId)
		WHERE MATCH (title) 
		AGAINST ('%s')
	GROUP BY BookId;`, book)
	results, err := db.Query(query)
	if err != nil {
		return nil, err
	}

	for results.Next() {
		var bookTitle util.MySQLResult
		err = results.Scan(&bookTitle.BookID,
			&bookTitle.Title,
			&bookTitle.Title1,
			&bookTitle.Lang,
			&bookTitle.Author,
		)
		log.Println(bookTitle.Title, bookTitle.Author)
		if err != nil {
			return queryResults, err
		}
		queryResults = append(queryResults, bookTitle)

	}

	if len(queryResults) > 1 {

		// Get best match books
		var wordsToTest []string
		for _, word := range queryResults {
			appendWord := fmt.Sprintf("%s %s", word.Title, word.Author)
			wordsToTest = append(wordsToTest, appendWord)
		}

		bestMatchBooks := util.GetBestMatch(originalQuery, wordsToTest)
		// log.Println("======================================================")
		// for _, match := range bestMatchBooks {
		// 	log.Println(match)
		// }
		// log.Println("======================================================")

		for _, word := range queryResults {
			var appended []string
			appendWord := fmt.Sprintf("%s %s", word.Title, word.Author)
			for _, wordInBestMatch := range bestMatchBooks {
				wordInBestMatchLower := strings.ToLower(wordInBestMatch)
				appendWordLower := strings.ToLower(appendWord)
				if strings.Contains(wordInBestMatchLower, appendWordLower) {
					if !stringInSlice(appendWordLower, appended) {
						log.Println(wordInBestMatch, word.Title)
						// if wordInBestMatch == word.Title {
						finalQueryResult = append(finalQueryResult, word)
						appended = append(appended, appendWordLower)
					}
				}
			}
		}
	} else {
		finalQueryResult = queryResults
	}

	return finalQueryResult, nil
}

func stringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}
