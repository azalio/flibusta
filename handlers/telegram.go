package handlers

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/azalio/flibusta/util"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/azalio/flibusta/db"
)

var (
	// Канал, куда будем помещать запросы к flibusta
	flibustaReq chan tgbotapi.Message
	// Канал, куда будем помещать ответы на запросы к flibusta
	flibustaRes chan util.FlibustaResponse
)

// GetConn - get connection to telegram
func getBot(t string) (*tgbotapi.BotAPI, error) {
	bot, err := tgbotapi.NewBotAPI(t)
	bot.Debug = false

	if err != nil {
		return nil, err
	}
	log.Printf("Authorized on account %s", bot.Self.UserName)

	return bot, err
}

// Telegram - work with telegram bot
func Telegram(token string) {
	bot, err := getBot(token)
	if err != nil {
		log.Println(err)
	}

	flibustaReq = make(chan tgbotapi.Message, 5)
	go getBooksFromDb()
	flibustaRes = make(chan util.FlibustaResponse, 5)

	u := tgbotapi.NewUpdate(0)

	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)

	for {
		select {
		case update := <-updates:
			if update.Message == nil { // ignore any non-Message Updates
				continue
			}
			UserName := update.Message.From.UserName
			UserID := update.Message.From.ID
			ChatID := update.Message.Chat.ID
			Text := update.Message.Text
			log.Printf("[%s] %d %d %s", UserName, UserID, ChatID, Text)
			// msg := tgbotapi.NewMessage(ChatID, "")
			// Выдаем запрошенный файл
			if strings.Contains(Text, "/start") {
				help := fmt.Sprintf("Бот ищет совпадения с названием книг.\nПосле названия книги вы можете указать автора, чтобы уточнить запрос.\n")
				msg := tgbotapi.NewMessage(ChatID, help)
				_, err = bot.Send(msg)
				if err != nil {
					log.Println(err)
				}
			} else if strings.Contains(Text, "/download") {
				log.Printf("Trying to download \n")
				msg := tgbotapi.NewMessage(ChatID, "Trying to download file")
				_, err = bot.Send(msg)
				if err != nil {
					log.Println(err)
				}
				bookArr := strings.Split(Text, "_")
				var bookID string
				var fileFormat string
				if len(bookArr) == 3 {
					bookID = bookArr[2]
					fileFormat = bookArr[1]
					bookname := bookID + "." + fileFormat
					url := fmt.Sprintf("%s/b/%s/%s",
						util.Config.Flibusta.SiteURL, bookID, fileFormat)
					log.Println(url)
					bookfile, err := util.DownloadFile(url)
					if err != nil && err.Error() == "Server return html page instead book" {
						log.Println("Try to download from onion")
						url = fmt.Sprintf("%s/b/%s/%s",
							util.Config.Flibusta.SiteURLOnion, bookID, fileFormat)
						log.Println(url)
						bookfile, err = util.DownloadFile(url)
					}
					if err != nil {
						log.Println(err)
						errorForUser := "При скачивании файла произошла ошибка."
						msg := tgbotapi.NewMessage(ChatID, errorForUser)
						_, err = bot.Send(msg)
						if err != nil {
							log.Println(err)
						}
					} else {
						bookfileReader, err := os.Open(bookfile)
						if err != nil {
							log.Println(err)
						}

						bookSizeInfo, err := os.Stat(bookfile)
						if err != nil {
							log.Println(err)
						}

						bookSize := bookSizeInfo.Size()
						msg := tgbotapi.NewDocumentUpload(ChatID, tgbotapi.FileReader{
							Reader: bookfileReader,
							Name:   bookname,
							Size:   bookSize,
						})
						_, err = bot.Send(msg)
						if err != nil {
							log.Println(err)
						}
					}

				}
			} else {
				// reply := fmt.Sprintf("Начинаю поиск, ожидайте.\n")
				// msg := tgbotapi.NewMessage(ChatID, reply)
				// _, err = bot.Send(msg)
				// if err != nil {
				// 	log.Println(err)
				// }
				flibustaReq <- *update.Message
				//reply = get_ig_popular()
				//msg.ReplyToMessageID = update.Message.MessageID
			}
		case resp := <-flibustaRes:
			numOfBooks := len(resp.Res)
			reply := fmt.Sprintf("Найдено <b>%d</b> \n", numOfBooks)
			for _, book := range resp.Res {
				// reply += book.Title + "(" + book.Title1 + ")" + " - " + book.Lang + " "
				// reply += book.FirstName + " " + book.MiddleName + " " + book.LastName + "\n"
				reply += "<b>" + book.Title + "</b> - " + book.Lang + " " + book.Title1 + "\n"
				reply += book.Author + "\n"
				// download_epub_453698
				reply += "Скачать книгу:\n"
				reply += fmt.Sprintf("/download_epub_%d\n", book.BookID)
				reply += fmt.Sprintf("/download_fb2_%d\n", book.BookID)
				reply += fmt.Sprintf("/download_mobi_%d\n", book.BookID)
			}

			msg := tgbotapi.NewMessage(resp.M.Chat.ID, reply)
			msg.ParseMode = "html"
			msg.ReplyToMessageID = resp.M.MessageID
			bot.Send(msg)
		}
	}

	// for update := range updates {
	// 	if update.Message == nil { // ignore any non-Message Updates
	// 		continue
	// 	}
	// 	UserName := update.Message.From.UserName
	// 	UserID := update.Message.From.ID
	// 	ChatID := update.Message.Chat.ID
	// 	Text := update.Message.Text
	// 	log.Printf("[%s] %d %d %s", UserName, UserID, ChatID, Text)

	// }

}

func getBooksFromDb() {
	for {
		for msg := range flibustaReq {
			books, err := db.Select(msg.Text)
			if err != nil {
				log.Println(err)
				continue
			}
			flibustaRes <- util.FlibustaResponse{books, msg}
		}
	}
}
