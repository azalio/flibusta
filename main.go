package main

import (
	"flag"
	"log"

	"gitlab.com/azalio/flibusta/handlers"
	"gitlab.com/azalio/flibusta/util"
)

func main() {
	configPtr := flag.String("config", "config.yaml", "path to config")
	update := flag.Bool("update", false, "update db")
	telegram := flag.Bool("telegram", false, "run telegram bot")

	flag.Parse()

	// get config
	util.InitConfig(*configPtr)

	// only update db and exit
	if *update {
		err := util.Update()
		if err != nil {
			log.Fatal(err)
		}
	}

	// run telegram handler
	if *telegram {
		log.Println("Run telegram bot")
		handlers.Telegram(util.Config.Telegram.Token)
	}

}
