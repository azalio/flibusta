package util

// import "github.com/schollz/closestmatch"
import (
	"sort"

	"github.com/lithammer/fuzzysearch/fuzzy"
)

// GetBestMatch - get list of string and return best match
func GetBestMatch(userString string, wordsToTest []string) []string {
	// bagSizes := []int{5}
	// cm := closestmatch.New(wordsToTest, bagSizes)
	// return cm.ClosestN(userString, 1)
	// matchList := fuzzy.FindFold(userString, wordsToTest)
	// log.Println(wordsToTest)
	matches := fuzzy.RankFindFold(userString, wordsToTest)
	sort.Sort(matches)
	// log.Println(matches)
	var result []string
	resLen := len(matches)
	if resLen > 0 {
		if resLen > 0 {
			resLen = 1
		}
		for _, match := range matches[:resLen] {
			result = append(result, match.Target)
		}
	}
	return result
}
