package util

import (
	"reflect"
	"testing"
)

func TestGetBestMatch(t *testing.T) {
	type args struct {
		userString  string
		wordsToTest []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "гарри поттер и методы",
			args: args{"гарри поттер и методы", []string{"Гарри Поттер и методы рационального мышления Юдковски"}},
			want: []string{"Гарри Поттер и методы рационального мышления Юдковски"},
		},
		{
			name: "над пропастью во ржи",
			args: args{"над пропастью во ржи", []string{"Прыжок над пропастью Питер  Джеймс",
				"Снежный мост над пропастью Валентина Николаевна Журавлева",
				"Снежный мост над пропастью Валентина Николаевна Журавлева",
				"Меч над пропастью Михаил  Ахманов", "Прыжок над пропастью Питер  Джеймс",
				"Над пропастью по лезвию меча Равиль Нагимович Бикбаев",
				"Над пропастью во ржи Джером Дейвид Сэлинджер",
				"Башня над пропастью Ян  Ирвин",
				"Над пропастью во лжи Юрий Маркович Нагибин",
				"Владимир Высоцкий. По-над пропастью Юрий Михайлович Сушко",
				"Над пропастью любви Борис Михайлович Майнаев",
				"Любовь и безумства поколения 30-х. Румба над пропастью Елена Владимировна Прокофьева",
				"Любовь и безумства поколения 30-х. Румба над пропастью Татьяна Викторовна Умнова",
				"Над пропастью во лжи Валерия Ильинична Новодворская",
				"Дефиле над пропастью Ольга Геннадьевна Володарская",
				"Над пропастью во лжи Светлана Владимировна Успенская",
				"Расправляя крылья: Шаг перед пропастью Сергей Анатольевич Кусков",
				"Над пропастью во сне: Мой отец Дж. Д. Сэлинджер Маргарет А Сэлинджер",
				"Поцелуй над пропастью Айрис  Джоансен",
				"Над пропастью Владимир Григорьевич Кренев",
				"Над пропастью не ржи! Татьяна Игоревна Луганцева",
				"Над пропастью Крис  Кассиди",
				"Танец над пропастью Ирина  Градова",
				"Шаг над пропастью Наталья Николаевна Александрова",
				"Псы над пропастью Сергей Васильевич Самаров",
				"Над пропастью жизнь ярче Анна и Сергей  Литвиновы",
				"Тропинка над пропастью Александр  Глазунов",
				"Над пропастью во ржи Игорь Олегович Гетманский",
				"Мир над пропастью Олег Юрьевич Рой",
				"Лик над пропастью Иван Иванович Любенко",
				"Снижаясь над пропастью (СИ) Михаил  Тимофеев (2)",
				"Тропинка над пропастью Александр Константинович Глазунов",
				"Встреча над пропастью Ольга Петровна Юнязова",
				"Над пропастью Михаил Петрович Старицкий",
				"Над пропастью во лжи Анатолий Яковлевич Гончаров",
				"Полет над пропастью Эльмира Анатольевна Нетесова",
				"Танго над пропастью Евгения Павловна Савицкая",
				"Над пропастью Шукур  Халмирзаев",
				"Мост над пропастью Артур Юрьевич Газаров",
				"Над пропастью во ржи Джером Дейвид Сэлинджер",
				"Колесо над пропастью Константин Борисович Шишкан"}},
			want: []string{"Над пропастью во ржи Джером Дейвид Сэлинджер"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetBestMatch(tt.args.userString, tt.args.wordsToTest); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetBestMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}
