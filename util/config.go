package util

import (
	"github.com/jinzhu/configor"
)

// Config - configuration
var Config = struct {
	APPName string `default:"app name"`

	DB struct {
		Name     string
		User     string `default:"root" env:"DBUser"`
		Password string `required:"true" env:"DBPassword"`
		Port     uint   `default:"3306" env:"DBPort"`
		Host     string `default:"localhost" env:"DBHost"`
	}

	UpdateUrls struct {
		BooksURL      string `default:"http://flibusta.is/sql/lib.libbook.sql.gz"`
		AuthorURL     string `default:"http://flibusta.is/sql/lib.libavtor.sql.gz"`
		AuthorNameURL string `default:"http://flibusta.is/sql/lib.libavtorname.sql.gz"`
	}

	Flibusta struct {
		SiteURL      string `default:"http://flibusta.is"`
		SiteURLOnion string `default:"http://flibustahezeous3.onion"`
		Login        string `default:"root"`
		Pass         string `default:"pass"`
		Cookie       string `default:"Hello" env:"Cookie"`
	}

	Telegram struct {
		Token string `required:"true" env:"TGToken"`
	}
}{}

// InitConfig init config
func InitConfig(cfg string) {
	configor.Load(&Config, cfg)
}
