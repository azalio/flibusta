package util

import (
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	// mysql driver
	_ "github.com/go-sql-driver/mysql"
)

// DownloadFile file from url
func DownloadFile(url string) (string, error) {
	filepath, err := ioutil.TempFile("", "flibusta")
	log.Printf("Start to download file %s from url %s\n", filepath.Name(), url)

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 YaBrowser/18.9.0.3363 Yowser/2.5 Safari/537.36")
	req.Header.Add("Cookie", Config.Flibusta.Cookie)
	resp, err := client.Do(req)

	// resp, err := http.Get(url)
	log.Println(resp.StatusCode)
	for header, value := range resp.Header {
		log.Println(header, value)
		if header == "Content-Type" {
			if strings.Contains(value[0], "text/html") {
				err = errors.New("Server return html page instead book")
			}
		}
	}
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	_, err = io.Copy(filepath, resp.Body)
	if err != nil {
		return "", err
	}

	return filepath.Name(), nil
}
