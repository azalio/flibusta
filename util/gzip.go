package util

import (
	"compress/gzip"
	"io"
	"io/ioutil"
	"os"
)

// UnGzip file
func UnGzip(filepath string) (string, error) {
	gzfile, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer gzfile.Close()

	srcFile, err := gzip.NewReader(gzfile)
	if err != nil {
		return "", err
	}
	defer srcFile.Close()

	// dstFile, err := os.OpenFile("file.sql", os.O_RDWR|os.O_CREATE, 0644)
	// if err != nil {
	// 	return nil
	// }
	// defer dstFile.Close()

	tmpfile, err := ioutil.TempFile("", "flibusta")
	// log.Println(tmpfile.Name())
	if err != nil {
		return "", err
	}
	defer tmpfile.Close()

	_, err = io.Copy(tmpfile, srcFile)
	if err != nil {
		return "", err
	}
	return tmpfile.Name(), nil
}
