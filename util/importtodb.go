package util

import (
	"fmt"
	"log"
	"os/exec"
)

// ImportToMySQL import sql file to MySQL db
func ImportToMySQL(filepath string, dbname string, dbuser string, dbpass string,
	dbport uint, dbhost string) error {
	log.Printf("Start to import file %s to db %s\n", filepath, dbname)

	// CREATE DATABASE IF NOT EXISTS flibusta
	cmd := exec.Command("mysql",
		fmt.Sprintf("-u%s", dbuser),
		fmt.Sprintf("-p%s", dbpass),
		fmt.Sprintf("--host=%s", dbhost),
		"-e",
		fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s CHARACTER SET utf8 COLLATE utf8_general_ci;", dbname))
	_, err := cmd.Output()
	if err != nil {
		return err
	}
	log.Println("Database created")

	cmd = exec.Command("mysql",
		fmt.Sprintf("-u%s", dbuser),
		fmt.Sprintf("-p%s", dbpass),
		fmt.Sprintf("--host=%s", dbhost),
		fmt.Sprintf("%s", dbname), "-e",
		fmt.Sprintf("source %s;", filepath))
	_, err = cmd.Output()
	if err != nil {
		return err
	}
	log.Println("Database imported")

	return nil
}

// AlterToMySQL - alter base
func AlterToMySQL(dbname string, dbuser string, dbpass string,
	dbport uint, dbhost string) error {
	log.Printf("Start to alter db %s\n", dbname)
	// cmd := exec.Command("mysql",
	// 	fmt.Sprintf("-u%s", dbuser),
	// 	fmt.Sprintf("-p%s", dbpass),
	// 	fmt.Sprintf("--host=%s", dbhost),
	// 	fmt.Sprintf("%s", dbname), "-e",
	// 	`DELETE FROM libbook WHERE Deleted<>0
	// 	OR (FileType<>"djvu"
	// 	AND FileType<>"pdf" AND FileType<>"doc"
	// 	AND FileType<>"fb2" AND FileType<>"epub"
	// 	AND FileType<>"mobi");`)
	// _, err := cmd.Output()
	// if err != nil {
	// 	return err
	// }
	// log.Println("Database cleaned")

	commands := []string{
		"ALTER DATABASE flibusta CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;",
		`DELETE FROM libbook WHERE Deleted<>0 
	    OR ( FileType<>"fb2" AND FileType<>"epub" AND FileType<>"mobi");`,
		"ALTER TABLE libbook CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
		"ALTER TABLE libavtor CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
		"ALTER TABLE libavtorname CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
		"ALTER TABLE libbook ADD FULLTEXT FULLTEXT_TITLE (title);",
		"ALTER TABLE `libbook` ADD INDEX `bookTitle` (`Title`);",
		"ALTER TABLE `libavtorname` ADD INDEX `avtorfirstname` (`FirstName`);",
		"ALTER TABLE `libavtorname` ADD INDEX `avtormiddlename` (`MiddleName`);",
		"ALTER TABLE `libavtorname` ADD INDEX `avtorlastname` (`LastName`);",
		"CREATE FULLTEXT INDEX libbook_title_full_text_idx ON libbook(Title);",
		"CREATE FULLTEXT INDEX libavtorname_lastname_full_text_idx ON libavtorname(LastName);",
		"CREATE FULLTEXT INDEX libavtorname_middlename_full_text_idx ON libavtorname(MiddleName);",
		"CREATE FULLTEXT INDEX libavtorname_firstname_full_text_idx ON libavtorname(FirstName);"}

	for _, command := range commands {

		log.Println(command)
		cmd := exec.Command("mysql",
			fmt.Sprintf("-u%s", dbuser),
			fmt.Sprintf("-p%s", dbpass),
			fmt.Sprintf("--host=%s", dbhost),
			fmt.Sprintf("%s", dbname), "-e",
			fmt.Sprintf("%s", command))
		_, err := cmd.Output()
		if err != nil {
			return err
		}
		log.Println("Database altered")
	}

	return nil
}
