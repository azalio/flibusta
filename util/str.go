package util

import (
	"strings"
)

// PlusString 'aaa' -> '+aaa'
func PlusString(str *string) {
	var resultString string
	words := strings.Fields(*str)
	for _, word := range words {
		resultString += "+" + word + " "
	}
	*str = resultString
}
