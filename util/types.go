package util

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

// MySQLResult - Result from MySQL query
type MySQLResult struct {
	BookID int
	Title  string
	Title1 string
	Lang   string
	Author string
}

// FlibustaResponse - Struct for flibusta
type FlibustaResponse struct {
	Res []MySQLResult
	M   tgbotapi.Message
}
