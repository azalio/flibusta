package util

import (
	"log"
	"os"
	"reflect"
)

// Update database
func Update() error {
	urls := Config.UpdateUrls
	v := reflect.ValueOf(urls)
	for i := 0; i < v.NumField(); i++ {

		url := v.Field(i).Interface()

		log.Printf("update flibusta db, url %s\n", url)

		fileSQL, err := DownloadFile(url.(string))
		if err != nil {
			return err
		}
		defer os.Remove(fileSQL)

		filepath, err := UnGzip(fileSQL)
		if err != nil {
			return err
		}
		defer os.Remove(filepath)

		err = ImportToMySQL(filepath, Config.DB.Name, Config.DB.User,
			Config.DB.Password, Config.DB.Port, Config.DB.Host)
		if err != nil {
			return err
		}

	}

	err := AlterToMySQL(Config.DB.Name, Config.DB.User,
		Config.DB.Password, Config.DB.Port, Config.DB.Host)
	if err != nil {
		return err
	}

	return nil
}
